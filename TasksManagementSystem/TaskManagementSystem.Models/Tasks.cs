﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using static TaskManagementSystem.Data.Helpers.Enums;

namespace TaskManagementSystem.Models
{
    public class Tasks
    {
        private ICollection<Comment> comments;

        public Tasks()
        {
            this.comments = new HashSet<Comment>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        [StringLength(5000, MinimumLength = 3, ErrorMessage = "The description length must be between {2} and {1} symbols.")]
        public string Description { get; set; }

        public DateTime CreatedDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? RequiredByDate { get; set; }

        public TaskStatus Status { get; set; }

        public TaskType Type { get; set; }

        public string AssignedTo { get; set; }

        public string AuthorName { get; set; }

        public virtual ICollection<Comment> Comments
        {
            get { return this.comments; }
            set { this.comments = value; }
        }
    }
}
