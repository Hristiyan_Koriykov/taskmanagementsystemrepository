﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using static TaskManagementSystem.Data.Helpers.Enums;

namespace TaskManagementSystem.Models
{
    public class Comment
    {
        [Key]
        public int Id { get; set; }

        public int TaskId { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        [StringLength(250, MinimumLength = 3, ErrorMessage = "The Content length must be between {2} and {1} symbols.")]
        public string Content { get; set; }

        public DateTime DateAdded { get; set; }

        [Display(Name = "Type")]
        public CommentType Type { get; set; }

        [Display(Name = "Reminder Date")]
        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? ReminderDate { get; set; }

        public virtual Tasks Task { get; set; }

        public string AuthorName { get; set; }
    }
}
