﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace TaskManagementSystem.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
