﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TaskManagementSystem.Data.Helpers
{
    public class Enums
    {
        public enum CommentType
        {
            Ordinary,
            Reminder
        }

        public enum TaskStatus
        {
            Open,
            Proceeding,
            Closed
        }

        public enum TaskType
        {
            Development,
            Test,
            Deploy,
            Bug
        }

    }
}
