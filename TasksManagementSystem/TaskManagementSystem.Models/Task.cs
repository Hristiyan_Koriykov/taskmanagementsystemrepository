﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using static TaskManagementSystem.Data.Helpers.Enums;

namespace TaskManagementSystem.Models
{
    public class Task
    {
        private ICollection<Comment> comments;
        private ICollection<ApplicationUser> users;

        public Task()
        {
            this.comments = new HashSet<Comment>();
            this.users = new HashSet<ApplicationUser>();
        }

        [Key]
        public int Id { get; set; }
       
        [StringLength(5000, MinimumLength = 3, ErrorMessage = "The description length must be between {2} and {1} symbols.")]
        public string Description { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime RequiredByDate { get; set; }

        public TaskStatus Status { get; set; }

        public TaskType Type { get; set; }

        public DateTime NextActionDate { get; set; }

        public virtual ApplicationUser Author { get; set; }

        public virtual ICollection<Comment> Comments
        {
            get { return this.comments; }
            set { this.comments = value; }
        }

        public virtual ICollection<ApplicationUser> Users
        {
            get { return this.users; }
            set { this.users = value; }
        }
    }
}
