﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TaskManagementSystem.Data;
using TaskManagementSystem.Models;
using TasksManagementSystem.ViewModels;
using static TaskManagementSystem.Data.Helpers.Enums;

namespace TasksManagementSystem.Controllers
{
    [Authorize]
    public class CommentsController : BaseController
    {
        // GET: /Comments/
        public ActionResult CommentsList()
        {
            var comments = this.Data.Comments.GetAll().ToList();
            return View(comments);
        }

        // GET: /Comments/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return null;
            }
            Comment comment = this.Data.Comments.GetById(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            return View(comment);
        }

        // POST: /Comments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Comment comment = this.Data.Comments.GetById(id);
            this.Data.Comments.Delete(comment);
            this.Data.SaveChanges();
            return RedirectToAction("TaskDetails", "Tasks", new { id= comment.TaskId});
        }

        // GET: /Comments/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return null;
            }
            Comment comment = this.Data.Comments.GetById(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            ViewBag.CommentType = new SelectList(Enum.GetValues(typeof(CommentType)), comment.Type);
            return View(comment);
        }

        // POST: /Comments/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Comment comment)
        {
            if (ModelState.IsValid)
            {
                comment.AuthorName = User.Identity.Name;
                comment.DateAdded = DateTime.Now;
                this.Data.Comments.Edit(comment);
                this.Data.SaveChanges();
                return RedirectToAction("TaskDetails", "Tasks", new { id = comment.TaskId });
            }
            ViewBag.CommentType = new SelectList(Enum.GetValues(typeof(CommentType)), comment.Type);
            return View(comment);
        }
    }
}