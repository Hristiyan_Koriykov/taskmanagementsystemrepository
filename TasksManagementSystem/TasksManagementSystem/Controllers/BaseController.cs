﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TaskManagementSystem.Data.UnitOfWork;

namespace TasksManagementSystem.Controllers
{
    [Authorize]
    public class BaseController : Controller
    {
        protected IUnitOfWork Data;

        public BaseController(IUnitOfWork data)
        {
            this.Data = data;
        }

        public BaseController()
            : this(new UnitOfWork())
        {
        }
    }
}