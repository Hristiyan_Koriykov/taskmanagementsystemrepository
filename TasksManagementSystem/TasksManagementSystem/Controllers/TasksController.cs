﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TaskManagementSystem.Models;
using TasksManagementSystem.ViewModels;
using static TaskManagementSystem.Data.Helpers.Enums;

namespace TasksManagementSystem.Controllers
{
    [Authorize]
    public class TasksController : BaseController
    {
        [HttpGet]
        public ActionResult Add()
        {
            List<SelectListItem> listOfUsers = new List<SelectListItem>();
            listOfUsers.Insert(0, (new SelectListItem { Text = "Ivan", Value = "0" }));
            listOfUsers.Insert(1, (new SelectListItem { Text = "Petyr", Value = "1" }));
            listOfUsers.Insert(2, (new SelectListItem { Text = "Hristiyan", Value = "2" }));

            TaskAddViewModel model = new TaskAddViewModel();        
            model.AssignedToList = listOfUsers;
            model.Statuses = LoadStatusesFromEnumeration();
            model.Types = LoadTypesFromEnumeration();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Create")]
        public ActionResult Add(TaskAddViewModel taskVM)
        {
            List<SelectListItem> listOfUsers = new List<SelectListItem>();
            listOfUsers.Insert(0, (new SelectListItem { Text = "Ivan", Value = "0" }));
            listOfUsers.Insert(1, (new SelectListItem { Text = "Petyr", Value = "1" }));
            listOfUsers.Insert(2, (new SelectListItem { Text = "Hristiyan", Value = "2" }));

            if (ModelState.IsValid && taskVM != null)
            {
                Tasks task = new Tasks();
                task.Title = taskVM.TaskTitle;
                task.AuthorName = User.Identity.Name;
                task.CreatedDate = DateTime.Now;
                task.RequiredByDate = taskVM.RequiredByDate;
                task.Status = (TaskStatus)Convert.ToInt32(taskVM.StatusId);
                task.Type = (TaskType)Convert.ToInt32(taskVM.TypeId);
                task.Description = taskVM.Description;
                task.AssignedTo = (from e in listOfUsers
                                   where e.Value == taskVM.AssignedToValue
                                   select e.Text
                                   ).FirstOrDefault();

                this.Data.Tasks.Add(task);
                this.Data.SaveChanges();

                return RedirectToAction("TasksList");
            }

            taskVM.Statuses = LoadStatusesFromEnumeration();
            taskVM.Types = LoadTypesFromEnumeration();
            taskVM.AssignedToList = listOfUsers;

            return View(taskVM);
        }

        public ActionResult TasksList()
        {
            var tasks = (from task in this.Data.Tasks.GetAll().ToList()
                         select new TaskHomeViewModel()
                         {
                             Id = task.Id,
                             RequiredByDate = task.RequiredByDate,
                             Status = task.Status,
                             Type = task.Type,
                             AssignedTo = task.AssignedTo,
                             TaskTitle = task.Title
                         });
            return View(tasks);
        }

        public ActionResult TaskDetails(int id)
        {
            var commentReminders = (from comment in this.Data.Comments.GetAll().ToList()
                                    where comment.TaskId == id && comment.ReminderDate != null
                                    select comment.ReminderDate
                         ).OrderBy(o=>o.Value);
            var viewModel = this.Data.Tasks.GetAll().Where(x => x.Id == id)
                .Select(x => new TaskDetailsViewModel
                {
                    Id = x.Id,
                    TaskTitle = x.Title,
                    CreatedDate = x.CreatedDate,
                    RequiredByDate = x.RequiredByDate,
                    Status = x.Status,
                    Type = x.Type,
                    AssignedTo = x.AssignedTo,
                    NextActionDate = commentReminders.FirstOrDefault(),
                    Description = x.Description,
                    CommentTypes = LoadCommentTypesFromEnumeration(),
                    AuthorName =  x.AuthorName,                  
                    Comments = x.Comments.Select(y => new CommentViewModel {
                        Id = y.Id,
                        AuthorUsername = y.AuthorName,
                        Content = y.Content,
                        DateAdded = y.DateAdded,
                        ReminderDate = y.ReminderDate,
                        Type = y.Type
                    }),
                }).FirstOrDefault();

            return View(viewModel);
        }

        [ValidateAntiForgeryToken]
        public ActionResult PostComment(SubmitCommentViewModel commentModel)
        {
            if (ModelState.IsValid)
            {           
                this.Data.Comments.Add(new Comment()
                {
                    AuthorName = User.Identity.GetUserName(),
                    Content = commentModel.Comment,
                    TaskId = commentModel.TaskId,
                    DateAdded = DateTime.Now,
                    ReminderDate = commentModel.CommentReminderDate,
                    Type = (CommentType)Convert.ToInt32(commentModel.CommentTypeId)
                });

                this.Data.SaveChanges();
                return RedirectToAction("TaskDetails", new { id = commentModel.TaskId } );
            }

            return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest, ModelState.Values.First().ToString());
        }

        public ActionResult Dashboard()
        {
            var tasks = (from task in this.Data.Tasks.GetAll().ToList()
                         select new TaskHomeViewModel()
                         {
                             Id = task.Id,
                             TaskTitle = task.Title,
                             RequiredByDate = task.RequiredByDate,
                             Status = task.Status,
                             Type = task.Type,
                             AssignedTo = task.AssignedTo
                         });
            return View(tasks);
        }

        #region LoadFromEnums
        private static List<SelectListItem> LoadStatusesFromEnumeration()
        {
            var statuses = new List<SelectListItem>();

            for (int i = 0; i < Enum.GetValues(typeof(TaskStatus)).Length; i++)
            {
                var item = new SelectListItem
                {
                    Text = ((TaskStatus)i).ToString(),
                    Value = (i).ToString()
                };

                if (((TaskStatus)i).ToString() == "Created")
                {
                    item.Selected = true;
                }

                statuses.Add(item);
            }

            return statuses;
        }

        private static List<SelectListItem> LoadTypesFromEnumeration()
        {
            var types = new List<SelectListItem>();

            for (int i = 0; i < Enum.GetValues(typeof(TaskType)).Length; i++)
            {
                var item = new SelectListItem
                {
                    Text = ((TaskType)i).ToString(),
                    Value = (i).ToString()
                };

                if (((TaskType)i).ToString() == "Development")
                {
                    item.Selected = true;
                }

                types.Add(item);
            }

            return types;
        }

        private static List<SelectListItem> LoadCommentTypesFromEnumeration()
        {
            var commentTypes = new List<SelectListItem>();

            for (int i = 0; i < Enum.GetValues(typeof(CommentType)).Length; i++)
            {
                var item = new SelectListItem
                {
                    Text = ((CommentType)i).ToString(),
                    Value = (i).ToString()
                };

                if (((CommentType)i).ToString() == "Ordinary")
                {
                    item.Selected = true;
                }

                commentTypes.Add(item);
            }

            return commentTypes;
        }

        #endregion
    }
}