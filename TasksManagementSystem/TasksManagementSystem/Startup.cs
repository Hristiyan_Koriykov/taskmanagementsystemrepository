﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TasksManagementSystem.Web.Startup))]
namespace TasksManagementSystem.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
