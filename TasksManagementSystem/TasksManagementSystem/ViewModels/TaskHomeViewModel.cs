﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using static TaskManagementSystem.Data.Helpers.Enums;

namespace TasksManagementSystem.ViewModels
{
    public class TaskHomeViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Title")]
        public string TaskTitle { get; set; }

        [Display(Name = "Required By Date")]
        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? RequiredByDate { get; set; }

        [DisplayName("Author")]
        public string AuthorName { get; set; }

        public TaskStatus Status { get; set; }

        [DisplayName("Status")]
        public string StatusString
        {
            get
            {
                return this.Status.ToString();
            }
        }

        public TaskType Type { get; set; }

        [DisplayName("Type")]
        public string TypeString
        {
            get
            {
                return this.Type.ToString();
            }
        }

        [DisplayName("Assigned To")]
        public string AssignedTo { get; set; }
    }
}