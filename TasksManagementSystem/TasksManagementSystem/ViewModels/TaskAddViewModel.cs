﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace TasksManagementSystem.ViewModels
{
    public class TaskAddViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Title")]
        public string TaskTitle { get; set; }

        public DateTime CreatedDate { get; set; }

        [Required]
        [Display(Name = "Required by date")]
        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? RequiredByDate { get; set; }

        [Required]
        [DisplayName("Status")]
        public int StatusId { get; set; }

        public IEnumerable<SelectListItem> Statuses { get; set; }

        [Required]
        [DisplayName("Type")]
        public int TypeId { get; set; }

        public IEnumerable<SelectListItem> Types { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Required]
        [DisplayName("Assigned to")]
        public string AssignedToValue { get; set; }

        public IEnumerable<SelectListItem> AssignedToList { get; set; }

    }
}