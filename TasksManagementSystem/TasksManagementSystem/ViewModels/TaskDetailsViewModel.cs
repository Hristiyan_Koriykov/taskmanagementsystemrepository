﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static TaskManagementSystem.Data.Helpers.Enums;

namespace TasksManagementSystem.ViewModels
{
    public class TaskDetailsViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Title")]
        public string TaskTitle { get; set; }

        [Display(Name = "Required by date")]
        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? RequiredByDate { get; set; }

        [Display(Name = "Created date")]
        public DateTime CreatedDate { get; set; }

        [Display(Name = "Created by")]
        public string AuthorName { get; set; }

        public TaskStatus Status { get; set; }

        public string StatusString
        {
            get
            {
                return this.Status.ToString();
            }
        }

        public TaskType Type { get; set; }

        public string TypeString
        {
            get
            {
                return this.Type.ToString();
            }
        }

        [Display(Name = "Assigned to")]
        public string AssignedTo { get; set; }

        [Display(Name = "Next action date")]
        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? NextActionDate { get; set; }


        public string Description { get; set; }

        public IEnumerable<CommentViewModel> Comments { get; set; }

        [Required]
        [DisplayName("Type")]
        public int CommentTypeId { get; set; }

        public IEnumerable<SelectListItem> CommentTypes { get; set; }

        [Display(Name = "Reminder date")]
        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? CommentReminderDate { get; set; }

        [Required]
        public string Comment { get; set; }
    }
}