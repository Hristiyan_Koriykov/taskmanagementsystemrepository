﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using static TaskManagementSystem.Data.Helpers.Enums;

namespace TasksManagementSystem.ViewModels
{
    public class CommentViewModel
    {
        public int Id { get; set; }

        public string AuthorUsername { get; set; }

        public DateTime DateAdded { get; set; }

        public string Content { get; set; }

        public CommentType Type { get; set; }

        [Display(Name = "Reminder date")]
        [Column(TypeName = "date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? ReminderDate { get; set; }
    }
}