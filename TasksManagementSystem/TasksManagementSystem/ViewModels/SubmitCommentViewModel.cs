﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using static TaskManagementSystem.Data.Helpers.Enums;

namespace TasksManagementSystem.ViewModels
{
    public class SubmitCommentViewModel
    {
        [Required]
        [StringLength(250, MinimumLength = 3, ErrorMessage = "The Content length must be between {2} and {1} symbols.")]
        public string Comment { get; set; }

        [Required]
        public int TaskId { get; set; }

        [Required]
        public CommentType CommentTypeId { get; set; }

        public DateTime? CommentReminderDate { get; set; }
    }
}