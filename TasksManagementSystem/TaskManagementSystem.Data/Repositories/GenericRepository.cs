﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskManagementSystem.Data.Abstract;

namespace TaskManagementSystem.Data.Repositories
{
    /// <summary>
    /// Generic repository which works for all entities
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {

        protected IDbSet<TEntity> DbSet { get; set; }
        protected DbContext Context { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public GenericRepository(DbContext context)
        {
            if (context == null)
                throw new ArgumentException("An instance of DbContext is required to use this repository.", "context");

            this.Context = context;
            this.DbSet = Context.Set<TEntity>();
        }

        /// <summary>
        /// Insert new entity in DB
        /// </summary>
        /// <param name="entity">New entity class</param>
        public void Add(TEntity entity)
        {
            DbEntityEntry entry = this.Context.Entry(entity);
            if (entry.State != EntityState.Detached)
            {
                entry.State = EntityState.Added;
            }
            else
            {
                this.DbSet.Add(entity);
            }
        }

        /// <summary>
        /// Delete entity by id
        /// </summary>
        /// <param name="Id"></param>
        public void Delete(int Id)
        {
            var entity = this.GetById(Id);
            if (entity != null)
            {
                this.Delete(entity);
            }
        }

        /// <summary>
        /// Delete entity
        /// </summary>
        /// <param name="entity">Entity to delete</param>
        public void Delete(TEntity entity)
        {
            DbEntityEntry entry = this.Context.Entry(entity);
            if (entry.State == EntityState.Deleted)
            {
                entry.State = EntityState.Deleted;
            }
            else
            {
                this.DbSet.Attach(entity);
                this.DbSet.Remove(entity);
            }
        }

        /// <summary>
        /// Get all entities
        /// </summary>
        /// <returns>List of entites</returns>
        public IList<TEntity> GetAll()
        {
            return this.DbSet.ToList();
        }

        /// <summary>
        /// Get entity by id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public TEntity GetById(int? Id)
        {
            return this.DbSet.Find(Id);
        }

        /// <summary>
        /// Edit/Update entity
        /// </summary>
        /// <param name="entity">Entity to update</param>
        public void Edit(TEntity entity)
        {
            DbEntityEntry entry = this.Context.Entry(entity);
            if (entry.State == EntityState.Detached)
            {
                this.DbSet.Attach(entity);
            }

            entry.State = EntityState.Modified;
        }

        /// <summary>
        /// Detach entity
        /// </summary>
        /// <param name="entity"></param>
        public virtual void Detach(TEntity entity)
        {
            DbEntityEntry entry = this.Context.Entry(entity);

            entry.State = EntityState.Detached;
        }

        #region IDisposable
        private bool disposedValue = false;

        /// <summary>
        /// Dispose instance
        /// </summary>
        /// <param name="disposing">Should dispose</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (!disposing)
                {
                    return;
                }

                if (Context == null)
                {
                    return;
                }

                Context.Dispose();
                Context = null;

                disposedValue = true;
            }
        }

        /// <summary>
        /// Dispose instance
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
