﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskManagementSystem.Data.Abstract
{
    /// <summary>
    /// Abstraction for repository
    /// </summary>
    ///  /// <typeparam name="TEntity"></typeparam>
    public interface IGenericRepository<TEntity> :IDisposable where TEntity:class
    {
        /// <summary>
        /// Gets all entities
        /// </summary>
        /// <returns>IEnumerable of entities</returns>
        IList<TEntity> GetAll();

        /// <summary>
        /// Gets entity by id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        TEntity GetById(int? Id);

        /// <summary>
        /// Adds entity to the context
        /// </summary>
        /// <param name="entity">Entity</param>
        void Add(TEntity entity);

        /// <summary>
        /// Edit entity on the context
        /// </summary>
        /// <param name="entity">Entity</param>
        void Edit(TEntity entity);

        /// <summary>
        /// Deletes entity from the context
        /// </summary>
        /// <param name="entity">Entity</param>
        void Delete(TEntity entity);

        /// <summary>
        /// Deletes entity by id
        /// </summary>
        /// <param name="Id"></param>
        void Delete(int Id);

        /// <summary>
        /// Detaches entity
        /// </summary>
        /// <param name="entity"></param>
        void Detach(TEntity entity);
    }
}
