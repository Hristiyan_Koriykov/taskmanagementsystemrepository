﻿using System;
using TaskManagementSystem.Data.Abstract;
using TaskManagementSystem.Models;

namespace TaskManagementSystem.Data.UnitOfWork
{
    /// <summary>
    /// Abstraction of Unit of work
    /// </summary>
    public interface IUnitOfWork : IDisposable
    {
        IGenericRepository<Tasks> Tasks { get; }

        IGenericRepository<Comment> Comments { get; }

        IGenericRepository<User> Users { get; }

        int SaveChanges();
    }
}
