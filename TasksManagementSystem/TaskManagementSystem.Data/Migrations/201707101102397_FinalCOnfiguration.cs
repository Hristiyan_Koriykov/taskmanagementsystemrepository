namespace TaskManagementSystem.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FinalCOnfiguration : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Tasks", "NextActionDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Tasks", "NextActionDate", c => c.DateTime(storeType: "date"));
        }
    }
}
