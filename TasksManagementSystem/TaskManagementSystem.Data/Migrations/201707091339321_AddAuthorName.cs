namespace TaskManagementSystem.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAuthorName : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Comments", "Author_Id", "dbo.Users");
            DropForeignKey("dbo.Tasks", "Author_Id", "dbo.Users");
            DropForeignKey("dbo.Users", "Tasks_Id", "dbo.Tasks");
            DropIndex("dbo.Comments", new[] { "Author_Id" });
            DropIndex("dbo.Users", new[] { "Tasks_Id" });
            DropIndex("dbo.Tasks", new[] { "Author_Id" });
            AddColumn("dbo.Comments", "AuthorName", c => c.String());
            AddColumn("dbo.Tasks", "AuthorName", c => c.String());
            DropColumn("dbo.Comments", "Author_Id");
            DropColumn("dbo.Users", "Tasks_Id");
            DropColumn("dbo.Tasks", "Author_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Tasks", "Author_Id", c => c.String(maxLength: 128));
            AddColumn("dbo.Users", "Tasks_Id", c => c.Int());
            AddColumn("dbo.Comments", "Author_Id", c => c.String(maxLength: 128));
            DropColumn("dbo.Tasks", "AuthorName");
            DropColumn("dbo.Comments", "AuthorName");
            CreateIndex("dbo.Tasks", "Author_Id");
            CreateIndex("dbo.Users", "Tasks_Id");
            CreateIndex("dbo.Comments", "Author_Id");
            AddForeignKey("dbo.Users", "Tasks_Id", "dbo.Tasks", "Id");
            AddForeignKey("dbo.Tasks", "Author_Id", "dbo.Users", "UserId");
            AddForeignKey("dbo.Comments", "Author_Id", "dbo.Users", "UserId");
        }
    }
}
