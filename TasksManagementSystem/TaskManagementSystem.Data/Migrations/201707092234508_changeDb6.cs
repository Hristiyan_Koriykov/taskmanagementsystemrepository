namespace TaskManagementSystem.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeDb6 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Comments", "ReminderDate", c => c.DateTime(storeType: "date"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Comments", "ReminderDate", c => c.DateTime(nullable: false, storeType: "date"));
        }
    }
}
